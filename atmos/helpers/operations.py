from typing import List
from pathlib import Path
from subprocess import run as cli_run
from contextlib import suppress
import tempfile, sys
import numpy as np
import atmos.assembly as atmos

def do_grimme_d3(assembly:atmos.Assembly, args:List[str]):
    from atmos.helpers.settings import grimme_d3_vars
    from atmos.helpers.constants import hartree_bohr2eV_angstroem
    from atmos.helpers.constants import hartree2eV
    
    grimme_d3_path = Path(grimme_d3_vars["path"])
    settings_args = grimme_d3_vars["args"].split()
    user_args = args
    dampings = ["-bj", "-zero", "-bjm", "-zerom"]
    found_user_damping = False
    for damping in dampings:
        if damping in user_args:
            found_user_damping = True # user has declared a damping function, use that!
    if found_user_damping:
        for damping in dampings:
            with suppress(ValueError):
                settings_args.remove(damping) # remove damping as found in settings.py
    for arg in user_args:
        if arg in settings_args:
            index_to_delete = settings_args.index(arg)
            if arg in ["-func", "-cutoff", "-cnthr"]:
                del settings_args[index_to_delete+1] # also delete the value set in settings.py
            del settings_args[index_to_delete] # delete the argument as set in settings.py
    d3_args = user_args + settings_args
    if "-noprint" in d3_args:
        d3_args.remove("-noprint")
    d3_xc = d3_args[d3_args.index("-func")+1]
    
    if (kind := assembly._has_quantity("information")) and kind == "scalar" and isinstance(assembly["information"], dict) and "xc_functional" in assembly["information"]: # check for FHI-aims PBE and HF functional
        assembly_xc = assembly["information"]["xc_functional"].lower()
        if d3_xc != assembly_xc:
            print(f"WARNING: Your assembly was calculated {assembly_xc} while Grimme's code is set to use {d3_xc}.")
    
    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        if assembly.dimensions is not None:
            assembly.writer(tmpdir/"POSCAR", format="vaspgeo")
            with suppress(ValueError):
                d3_args.remove("-grad")
                d3_args.remove("-pbc")
            process = cli_run([str(grimme_d3_path), str("POSCAR")]+d3_args+["-grad","-pbc"], capture_output=True, cwd=tmpdir)
        else:
            assembly.writer(tmpdir/"atmos_d3.XYZ", format="xyz", overwrite=True) # overwrite = True, just in case
            with suppress(ValueError):
                d3_args.remove("-grad")
            process = cli_run([str(grimme_d3_path), str("atmos_d3.XYZ")]+d3_args+["-grad"], capture_output=True, cwd=tmpdir)
        output = process.stdout.decode(sys.stdout.encoding).strip().split('\n')
        if "normal termination of dftd3" in output[-1]:
            d3_forces = np.loadtxt((tmpdir/"dftd3_gradient"))
            if assembly.dimensions is not None:
                d3_cell_forces = np.loadtxt((tmpdir/"dftd3_cellgradient"))
    if "normal termination of dftd3" in output[-1]:
        try:
            d3_energy = float(output[-13].split()[4])
        except ValueError:
            d3_energy = float(output[-13].split()[3]) * hartree2eV
        except IndexError:
            d3_energy = float(output[-12].split()[3]) * hartree2eV
        
        may_exist = ["grimme_d3~forces", "grimme_d3~forces_cell", "grimme_d3~energy" , "grimme_d3~energy_unit"]
        for quan in assembly.quantities: # remove already existing quantities of the same name
            if quan in may_exist:
                assembly.detach(quan)
        
        if (kind := assembly._has_quantity("energies")) and kind == "scalar" and isinstance(assembly["energies"], dict): # check for pre-existing data structure
            assembly["energies"]["grimme_d3"] = d3_energy
        else:
            assembly.attach("scalar", "grimme_d3~energy", d3_energy)
        if (kind := assembly._has_quantity("unit")) and kind == "scalar" and isinstance(assembly["unit"], dict) and "energies" in assembly["unit"] and isinstance(assembly["unit"]["energies"], dict):
            assembly["unit"]["energies"]["grimme_d3"] = "eV" # ^ ^ ^ ^ ^ again in line above, check for pre-existing data structure
        else:
            assembly.attach("scalar", "grimme_d3~energy_unit", "eV")
        
        assembly.attach("n_vector", "grimme_d3~forces", d3_forces, dtype=float)
        assembly["grimme_d3~forces"] *= hartree_bohr2eV_angstroem
        assembly["grimme_d3~forces"].attrs["unit"] = "eV/angstroem"
        
        if assembly.dimensions is not None:
            assembly.attach("cell", "grimme_d3~forces_cell", d3_cell_forces, dtype=float)
            assembly["grimme_d3~forces_cell"] *= hartree_bohr2eV_angstroem
            assembly["grimme_d3~forces_cell"].attrs["unit"] = "eV/angstroem"
    else:
        print("WARNING: DFT-D3 calculation failed!")

def interpret(assembly:atmos.Assembly, input_str:str) -> None:
    """An interpreter-like interface to perform quick operations on *assembly*.
    
    Commands and range of *assembly* to operate upon (here called selection) is separated by a comma.
    For example::

        my_assembly << move 3, x 3

    would move the atom number 4 in x direction by 3 Å. Here the string `move 3` is the 
    command and its argument(s) while after the portion after the comma i.e. `x 3`
    is the selection on which the command operates. This latter portion 
    is interpreted by the :func:`atmos.assembly.Assembly._process_idx` function.

    Other examples of the command and selections are::

        my_assembly << "move 5"        # moves all the atoms by  5 Å in each of the three directions
        my_assembly << "move -5, z"    # moves all the atoms by -5 Å in the z direction
        my_assembly << "move 5, x 3"   # moves the 4th atom  by  5 Å in the x direction
        my_assembly << "freeze"        # freeze all the atoms   (constrain their relaxation)
        my_assembly << "unfreeze"      # unfreeze all the atoms (unconstrain their relaxation)
        my_assembly << "freeze, x"     # freeze all the atoms along the x direction
        my_assembly << "unfreeze, z 5" # unfreeze atom number 6 along the z direction
        my_assembly << "fractional"    # convert positions into fractional coordinate system
        my_assembly << "cartesian"     # convert positions into cartesian coordinate system
        my_assembly << "translate_into_positives"  # move the atoms so that all are in the (+,+,+) quadrant
        my_assembly << "grimme_d3"     # do Grimme's D3 calculation on the system
    
    Parameters
    ----------
    input_str : str
        One of these: 

            - "move" : to move atoms (takes 1 argument i.e. magnitude by which to move)
            - "freeze" : to freeze atoms (constrain relaxation) (take no arguments)
            - "unfreeze" : to unfreeze atoms (take no arguments)
            - "fractional" : to turn position into fractional coordinates (take no arguments) 
            - "cartesian"  : to turn position into cartesian  coordinates (take no arguments)
            - "translate_into_positives" : to move the atoms so that all are in the (+,+,+) quadrant (take no arguments)
            - "grimme_d3": do Grimme's D3 calculation on the system. Path to the executable must be provided in settings.py

        followed by argument(s), comma, and the selection string.

    Raises
    ------
    SyntaxError
        If wrong syntax used for command.
    ValueError
        If trying to set improper combination of values.
    TypeError
        If trying to do operations on unsupported 'quantities' e.g. freeze on forces.
    """
    #                     action                    : need idx                    
    valid_actions      = {"move"                    : True,
                          "freeze"                  : True,
                          "unfreeze"                : True,
                          "fractional"              : False,
                          "cartesian"               : False,
                          "translate_into_positives": False,
                          "grimme_d3"               : False}
    expected_arguments = {"move"                    : [float],
                          "freeze"                  : [],
                          "unfreeze"                : [],
                          "fractional"              : [],
                          "cartesian"               : [],
                          "translate_into_positives": [],
                          "grimme_d3"               : []}
    expected_quantity  = {"move"                    : "positions",
                          "freeze"                  : "fixations",
                          "unfreeze"                : "fixations",
                          "fractional"              : "positions",
                          "cartesian"               : "positions",
                          "translate_into_positives": "positions",
                          "grimme_d3"               : "positions"}
    
    input_str = input_str if "," in input_str else input_str+","
    command, idx = input_str.lower().split(',', maxsplit=1)
    
    command = command.split()
    if not command or not command[0] in valid_actions.keys():
        raise SyntaxError(f"Perform one of these actions with << operator: {', '.join(valid_actions.keys())}")
    action = command.pop(0)
    if action == "grimme_d3": # special treatment for grimme_d3 because we want all the arguments passed to us as they are set by user
        expected_arguments["grimme_d3"] = [str]*len(command)
    if len(command) == len(expected_arguments[action]):
        args = []
        for i, arg in enumerate(command):
            try:
                args.append(expected_arguments[action][i](arg)) # cast and store
            except TypeError:
                raise TypeError(f"{action} expects an argument compatible with {expected_arguments[action][i]} here, instead got {arg}.")
    else:
        expected = expected_arguments[action] if expected_arguments[action] else "Nothing!"
        command_is = command if command else "Nothing!"
        raise SyntaxError(f"{action} has less/more arguments than expected.\nExpects: {expected}. Found: {command_is}.\nRemember you need to use a comma (,) to separate command from selection.")

    _, quantity, selection, direction = assembly._process_idx(idx)
    
    # compatibility with _process_idx
    if quantity not in idx:  # i.e. quantity was not defined by user. This means _process_idx has returned "atoms" or "positions". Ignore and pick the sensible one.
        quantity = expected_quantity[action]
        idx = quantity + " " + idx.strip() # make the quantity to operate upon explicit
    if quantity != expected_quantity[action]:
        raise TypeError(f"Can't perform {action} action on {quantity}.")
    if not valid_actions[action] and (direction or selection): # i.e. command does not need an idx but something was given
        print(f"{action} does not accept any selection. Ignoring: {idx}.")

    # sanity checks
    if action in ["fractional", "cartesian"]:
        if not assembly._has_quantity("positions") or assembly.dimensions is None:
            raise ValueError(f"Can't perform {action} operation on assembly without \"positions\" and/or \"dimensions\".")
    if action in ["freeze", "unfreeze"]:
        if not assembly._has_quantity("fixations"):
            assembly.attach("n_vector", "fixations", None, dtype=bool)
            print("x[\"fixations\"] are now defined.")
    if action in ["translate_into_positives", "grimme_d3"]:
        if not assembly._has_quantity("positions"):
            raise ValueError("Can't perform {action} operation on assembly without \"positions\"  defined.")
    
    # Perform action
    if action == "move":
        assembly[idx] += args[0]
    if action == "fractional":
        if not "coord_system" in assembly["positions"].attrs: # if it's done the first time
            print("\"coord_system\" is not an attribute of \"positions\". Assuming it was \"cartesian\" and converting it \"fractional\".")
            assembly["positions"].attrs["coord_system"] = "cartesian"
        if  assembly["positions"].attrs["coord_system"] == "cartesian":
            pos = assembly["positions"].data
            dim = assembly.dimensions
            assembly["positions"] = (np.linalg.inv(np.array(dim).T) @ np.array(pos).T).T
            assembly["positions"].attrs["coord_system"] = "fractional"
        else:
            print("The coordinates are already in the fractional system. Ignoring command.")
    if action == "cartesian":
        if not "coord_system" in assembly["positions"].attrs: # if it's done the first time
            print("\"coord_system\" is not an attribute of \"positions\". Assuming it was \"fractional\" and converting it \"cartesian\".")
            assembly["positions"].attrs["coord_system"] = "fractional"
        if  assembly["positions"].attrs["coord_system"] == "fractional":
            assembly["positions"] = assembly["positions"].data @ assembly.dimensions
            assembly["positions"].attrs["coord_system"] = "cartesian"
        else:
            print("The coordinates are already in the cartesian system. Ignoring command.")
    if action == "freeze":
        assembly[idx] = np.ones_like(assembly[idx].data, dtype=bool)
    if action == "unfreeze":
        assembly[idx] = np.zeros_like(assembly[idx].data, dtype=bool)
    if action == "translate_into_positives":
        for direction in ['x','y','z']:
            assembly << f"move {-assembly[f'positions {direction}'].data.min()}, {direction}"
    if action == "grimme_d3":
        do_grimme_d3(assembly, args)