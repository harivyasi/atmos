import multiprocessing, subprocess, tempfile
import numpy as np
from pathlib import Path
from multiprocessing import shared_memory
from os import name as os_name
from atmos.helpers.constants import bohr2angstroem, angstroem2bohr
from atmos.helpers.settings import big_file_MBs_to_read, parallel_cube_write
import atmos.assembly as atmos

def cube_parser(filepath:Path, quantity:str = "unknown", attach_voxeldat:bool = False) -> atmos.Assembly:    
    """To parse Gaussian Cube files.

    Format specification is `available here <https://h5cube-spec.readthedocs.io/en/latest/cubeformat.html>`_. 
    Cube files with a negative number of atoms and dataset-ids are unsupported.

    This function creates a new *assembly* object. To attach (e.g. hartree potential) 
    data to an existing *assembly* do::

        my_assembly.attach("voxeldat","potential","/path/to/hartree_potential.cube")
    
    Note that ATMOS handles datapoints (voxels) read from a cube file as float32
    to save on memory.

    Parameters
    ----------
    filepath : Path
        Path to the cube file that needs to be read.
    quantity : str, optional
        By default "unknown". A name for the quantity that is stored in the 
        cube file. Useful if there is information about charge on each atom in 
        the cube file or if `attach_voxeldat` is True.
    attach_voxeldat : bool, optional
        By default False.
        Attach data voxels and related information to the returned *assembly*.
        Attached data takes the form: 

            -  assembly["atoms"]                       of kind "atoms"
            -  assembly["positions"]                   of kind "n_vector"
            -  assembly["``quantity``"],               of kind "voxeldat"
            -  assembly["``quantity``\~cube_charges"], of kind "n_scalar"
            -  assembly["``quantity``\~cube_comment"], of kind "scalar"
            -  assembly["``quantity``\~cube_origin"],  of kind "vector"
            -  assembly["``quantity``\~cube_n_voxel"], of kind "vector"
            -  assembly["``quantity``\~cube_spacing"]. of kind "cell"
        
    Returns
    -------
    atmos.Assembly
        *assembly* with data from the cube file.

    Raises
    ------
    ValueError
        Raised if format of cube file is not supported.
    """
    comment = ""
    header  = []
    charges = []
    with open(filepath, "r") as stream:
        for i in range(2): # read the 2 comment lines
            comment += stream.readline()
        for i in range(4): # read the 4 header lines
            header.append(stream.readline().strip('\n'))
        header = np.array([line.split()[:4] for line in header], dtype=float)
        if int(header[0][0]) < 0:
            raise ValueError("Cube files with a negative number of atoms is unsupported.")
        assembly = atmos.Assembly(int(header[0][0])) # header[0,0] stores the number of atoms
        for i in range(len(assembly)):
            line = stream.readline().split()
            assembly[i] = (line[0])
            charges.append(line[1])
            assembly[f"{i} positions"] = line[2:5]
        assembly["positions"] *= bohr2angstroem
        assembly["positions"].attrs["unit"] = "angstroem"
        assembly["positions"].attrs["coord_system"] = "cartesian"
        if attach_voxeldat:
            # Reading data is contrived because we want to be memory conservative
            # and not hold the entire cube file in memory, it can run into GBs.
            # We have to hold its numpy copy (called voxels): no choice there.
            # However reading the file line by line is too slow with Python's lazy
            # loading. On the other hand, reading in segments as done here
            # risks splitting numbers in the middle. We take care of this by 
            # assuming that the last number is always split and combining it with
            # the next block of text... unless the next block (a) is not there i.e.
            # the file has ended, or (b) begins with a whitespace i.e. the split
            # occurred between two numbers.
            # 
            # NB (SSH): In my tests with mmap, this way of doing it is nearly as 
            # good as mmap on Windows and Linux. However mmap is around 2.5 times
            # worse on WSL.
            # 
            shape = tuple(header[1:,0].astype(int).flatten())
            voxels = np.empty(np.product(shape), dtype = 'float32')
            index = 0
            left_over = None
            while data := stream.read(big_file_MBs_to_read*1024*1024):
                if data[0].isspace() and left_over:
                    voxels[index] = left_over
                    left_over = None
                    index += 1
                data = data.split()
                if left_over:
                    data[0] = left_over+data[0]
                left_over = data.pop(-1)
                data = np.array(data, dtype = 'float32').flatten()
                len_data = len(data)
                voxels[index:index+len_data] = data
                index += len_data
                print(f"Read {index*100/np.product(shape):>5.1f}% of {quantity} data.")
            voxels[-1] = left_over
            voxels = voxels.reshape(shape)
            assembly.attach("scalar", f"{quantity}~cube_comment", comment)
            assembly.attach("vector", f"{quantity}~cube_origin",  header[0, 1:4], dtype=float)
            assembly.attach("vector", f"{quantity}~cube_n_voxel", header[1:, 0].astype(int), dtype=int)
            assembly.attach("cell",   f"{quantity}~cube_spacing", header[1:4, 1:4], dtype=float)
            assembly.attach("voxeldat", quantity, voxels, dtype='float32')
        charges = np.array(charges, dtype=float)
        if charges.any():
            # We stored charges only if non-zero charges are read.
            # Many programs write out zeros instead of actual charge data
            # in order to fit the format of a cube file. If it's all zeros, we 
            # can safely assume that the data is meaningless.
            assembly.attach("n_scalar", f"{quantity}~cube_charges", charges, dtype='float')
    return assembly

def _parallel_cube_write(pickled_data:tuple[int, str, np.dtype, tuple[int,int,int]]) -> None:
    """To write a block of data in a temporary file.

    This function is called many times, *in parallel*, to write temporary files, 
    each containing a block of data (to be merged into a single cube file later). 
    The data to be written is not sent directly. Instead it is made available using 
    a SharedMemory object. To interpret this data (which we know is a numpy array),
    we need its dtype and its shape. This information, the name of the temporary 
    file to be written and the number of the data block (index of axis=0 of the 
    numpy array) to be written is received by this function as pickled_data. Name 
    of the shared memory object ("voxels_to_write") is hard-coded to avoid 
    errors.

    Parameters
    ----------
    pickled_data : tuple[int, str, np.dtype, tuple[int,int,int]]
        A tuple of the following:

            - int : index of the data block to be written (index of axis=0 of the numpy array having voxel data).
            - str : path of the temporary file (as str and not as Path).
            - np.dtype : dtype of the numpy array that resides in the SharedMemory oject.
            - tuple[int,int,int] : shape of the same numpy array, expressed as a tuple.
    
    """
    i, tmp_name, dtype, shape = pickled_data
    from multiprocessing import shared_memory
    # prepare
    existing_shm = shared_memory.SharedMemory(name="voxels_to_write")
    voxels_to_write = np.ndarray(shape, dtype=dtype, buffer=existing_shm.buf)
    # write
    with open(tmp_name, 'w', encoding='UTF-8') as stream:
        for j in range(shape[1]):
            line_width = 6 # number of voxel data points in a line
            num_of_full_lines = len(voxels_to_write[i][j]) // line_width
            make_sci_str = lambda arr : np.array([np.format_float_scientific(num, precision=8, trim='.', exp_digits=1) for num in arr], dtype=str)
            arr_to_write = voxels_to_write[i][j][:num_of_full_lines*line_width]
            arr_to_write = make_sci_str(arr_to_write).reshape(num_of_full_lines,line_width)
            last_line    = voxels_to_write[i][j][num_of_full_lines*line_width:]
            last_line    = make_sci_str(last_line)
            content      = "\n".join([" ".join(line) for line in arr_to_write]+[" ".join(last_line)])+"\n"
            stream.write(content)
    # clean up
    del voxels_to_write
    existing_shm.close()

def cube_saver(filepath:Path, assembly:atmos.Assembly, quantity:str) -> None:
    """To write out a cube file.

    The function employs parallelization over (cpu_count - 2) processors 
    as long as there are a total of 4 processors. Parallelization is trivial. 
    We write out the cube file's comment, header and atom data serially to a
    file. Then we write out the data voxels in parallel to a number of temporary 
    files. Once written we combine these files using a command that runs 
    outside python (hence the speed). Our rudimentary tests show that an speed up of 
    ~6 times is achieved for a cube file of ~0.5 GB. At present 'posix' (OS X and
    most linux distros) and 'nt' (Windows 7/8/10 (CMD, Powershell)) systems are
    supported. The function fall backs on usual (slow speed) write utility 
    of python for smaller files and for unsupported OSes.

    Refer to the source code for extensive comments.

    Parameters
    ----------
    filepath : Path
        Path of the cube file to be written.
    assembly : atmos.Assembly
        *assembly* having the "voxeldat"-kind of data to be written.
    quantity : str
        Name of the "voxeldat"-kind of data to be written.

    Raises
    ------
    KeyError
        Raised if one of these is not present: "``quantity``\~cube_charges", 
        "``quantity``\~cube_comment", "``quantity``\~cube_origin", 
        "``quantity``\~cube_n_voxel" and "``quantity``\~cube_spacing". 
        This data is generally acquired when the cube file was read in. However 
        it is not difficult to invent it if you know what you are doing. A full 
        description of the cube file format is available 
        `here for help <https://h5cube-spec.readthedocs.io/en/latest/cubeformat.html>`_. 
    

    """
    for var in ["cube_comment", "cube_origin", "cube_n_voxel", "cube_spacing"]:
        if not assembly._has_quantity(f"{quantity}~{var}"):
            raise KeyError("Can't save {quantity} because {var} is missing.")
    comment = assembly[f"{quantity}~cube_comment"]
    num_lines_in_comment = len(comment.splitlines())
    if num_lines_in_comment != 2:
        if num_lines_in_comment == 0:
            comment = " CUBE FILE of {quantity} written by ATMOS\n"
        if num_lines_in_comment  < 2: # i.e. either 0 or 1
            comment += "\n" if not comment.endswith('\n') else "" # ensure first line ends with \n
            comment += f"{'*'*(len(comment)-1)}\n" # add an underline to the first line of comment
        else:
            comment = comment.splitlines()
            removed = "\n".join(comment[2:])
            comment = "\n".join(comment[:2])
            print(f"Comment of the cube file must be 2 lines long; removed following information: {removed}") # TODO: Move to UI
    comment += "\n" if not comment.endswith('\n') else "" # ensure second line ends with \n
    num_atoms = len(assembly)
    origin    = assembly[f"{quantity}~cube_origin"].data
    spacing   = assembly[f"{quantity}~cube_spacing"].data
    num_voxel = assembly[f"{quantity}~cube_n_voxel"].data
    content  = comment
    content += f"{num_atoms:5d}  {origin[0]:10.6f}  {origin[1]:10.6f}  {origin[2]:10.6f}\n"
    content += f"{num_voxel[0]:5d}  {spacing[0][0]:10.6f}  {spacing[0][1]:10.6f}  {spacing[0][2]:10.6f}\n"
    content += f"{num_voxel[1]:5d}  {spacing[1][0]:10.6f}  {spacing[1][1]:10.6f}  {spacing[1][2]:10.6f}\n"
    content += f"{num_voxel[2]:5d}  {spacing[2][0]:10.6f}  {spacing[2][1]:10.6f}  {spacing[2][2]:10.6f}\n"
    atoms = assembly.atoms["Zs"]
    if "unit" in assembly["positions"].attrs and assembly["positions"].attrs["unit"] == "angstroem":
            positions = assembly["positions"].data*angstroem2bohr
    charges = assembly[f"{quantity}~cube_charges"] if assembly._has_quantity(f"{quantity}~cube_charges") else np.zeros(len(assembly))
    for i in range(len(assembly)):
        content += f"{atoms[i]:5d}  {charges[i]:10.6f}  {positions[i][0]:10.6f}  {positions[i][1]:10.6f}  {positions[i][2]:10.6f}\n"
    with open(filepath, 'w') as stream:
        stream.write(content) # write out comment, header and atom data
    shape = assembly[quantity].data.shape
    dtype = assembly[quantity].data.dtype
    if np.product(shape) > 1000000 and os_name in ['nt','posix'] and multiprocessing.cpu_count() > 3 and parallel_cube_write:
        # Go parallel only if voxel-data matrix has more than 100*100*100 elements, it is a recognized OS and at least 4 processors.
        # This is a guessed optimum value, actual value needs testing.
        # In contrast to previous approach (global variable to share data), we are using
        # shared memory because global variables we bugging out in Windows implementation.
        # 
        # 1. create a data object that would be shared by all parallel processes.
        shm = shared_memory.SharedMemory(name="voxels_to_write", create=True, size=assembly[quantity].data.nbytes)
        shared_voxels = np.ndarray(shape, dtype=dtype, buffer=shm.buf) # np array stored in the shared data object
        shared_voxels[:] = assembly[quantity].data[:] # copy data to the shared data numpy array
        # 2. Ready temp files to write data to.
        tmp_files = [] 
        pickled_data = [] # information, other than voxel-data, that every process must receive.
        for i in range(shape[0]): # one file/process for every 'i' array in voxel-data
            tmp = tempfile.NamedTemporaryFile(mode='w',encoding='UTF-8',delete=False)
            tmp_files.append(tmp.name) # create a bunch of temp files, and get their location
            pickled_data.append((i, tmp_files[i], dtype, shape))
            # send process number (i), temp file location, and dtype+shape of the shared data object to each process
            tmp.close()
        # 3. Ready command to merge all temp files. This is equivalent to using
        # cat file1 file2 file3 ... file_i_ >> resulting_big_cube_file in the shell
        if os_name == "posix":
            commands = [f"cat {' '.join(tmp_files)} >> {filepath}"]
        else: # i.e. 'nt' i.e. Windows
            commands = []
            # Windows allows commands with a maximum of 8191 characters. (see http://support.microsoft.com/kb/830473)
            # So we will limit our command string length to roughly 8000.
            # The starting 5 characters are consumed by the command `type ` 
            # where type is windows equivalent for the `cat` command.
            tmpf = tmp_files[:]
            comm = [] # half-baked command, containing paths to temp files
            len_comm = 0
            len_command = 8000 - len(str(filepath)) - 10 # 10 for command name (type) and spaces and >>
            while tmpf:            
                comm.append(tmpf.pop(0))
                len_comm += len(comm[-1])+1 # plus one for the space that will be inserted later using str.join
                if len_comm >= len_command:
                    commands.append(f"type {' '.join(comm)} >> {filepath}")
                    comm, len_comm = [], 0 # reset comm and len_comm
            else:
                if comm: # attach all the remaining temp file paths as a new command in commands
                    commands.append(f"type {' '.join(comm)} >> {filepath}")
            for command in commands: # assert check because failure means logical error in code.
                assert len(command) < 8192, "This will lead to failure when executing on Windows. Please report."
        with multiprocessing.Pool(multiprocessing.cpu_count()-2) as pool: # leave 2 free so as to not halt the machine
            pool.map(_parallel_cube_write, pickled_data) # run in parallel to write to temp files
        for command in commands:
            subprocess.run(command, shell=True, capture_output=True) # combine temp files into one single file
        for tmpf in tmp_files:
            Path(tmpf).unlink() # delete temp files manually because delete=False when created, see above
        del shared_voxels
        shm.close()
        shm.unlink() # remove the shared memory object
    else: # for smaller files, if we can't recognize the OS as posix or windows or if num of processors is < 4.
        with open(filepath, 'a') as stream:
            voxels_to_write = assembly[quantity].data
            for i in range(shape[0]):
                for j in range(shape[1]):
                    line_width = 6 # number of voxel data points in a line
                    num_of_full_lines = len(voxels_to_write[i][j]) // line_width
                    make_sci_str = lambda arr : np.array([np.format_float_scientific(num, precision=8, trim='.', exp_digits=1) for num in arr], dtype=str)
                    arr_to_write = voxels_to_write[i][j][:num_of_full_lines*line_width] # split and rearrange the array
                    arr_to_write = make_sci_str(arr_to_write).reshape(num_of_full_lines,line_width)
                    last_line    = voxels_to_write[i][j][num_of_full_lines*line_width:] # make them into scientific notation
                    last_line    = make_sci_str(last_line)
                    content      = "\n".join([" ".join(line) for line in arr_to_write]+[" ".join(last_line)])+"\n"
                    stream.write(content)