import numpy as np
from pathlib import Path
from typing import Dict, Optional, Union
import atmos.formats.fhiaims as atmos_fhiaims
import atmos.formats.cube as atmos_cube
import atmos.formats.vasp as atmos_vasp
import atmos.formats.xyz as atmos_xyz
import atmos.assembly as atmos
# if you add more here, remember to modify line number in docstring of :func:`reader`

#                   keys                     values
valid_filekinds = {"output.aims"          : "aimsout",
                   "geometry.in.next_step": "aimsgeo",
                   ".in"                  : "aimsgeo",
                   ".npy"                 : "npy",
                   ".npz"                 : "npz",
                   ".cube"                : "cube",
                   ".cub"                 : "cube",
                   ".xyz"                 : "xyz",
                   "POSCAR"               : "vaspgeo"
                   }
                   # if you add more here, remember to modify line number in docstring of :func:`reader`

def _identify_filepath(filepath:Union[str, Path], format:Optional[str]):
    try:
        filepath = Path(filepath)
    except:
        raise ValueError(f"Expected a path or path-like object instead of {type(filepath)}.")
    if format is None:
        if filepath.name in valid_filekinds.keys():
            format = valid_filekinds[filepath.name]
        elif filepath.suffix.lower() in valid_filekinds.keys():
            format = valid_filekinds[filepath.suffix]
        elif filepath.suffix.lower() == ".nc":
            raise ValueError("Please use Assembly.load() / assembly.save() to load/save Assembly data.")
        else:
            raise ValueError(f"Couldn't identify format for file: {filepath}")
    else:
        if format in valid_filekinds.values():
            pass
        elif filepath.suffix == ".nc":
            raise ValueError("Please use Assembly.load() / assembly.save() to load/save Assembly data.")            
        else:
            raise ValueError(f"Couldn't identify format: {format}")
    return filepath, format

def  reader(filepath:Union[str, Path], format:str = None) -> Union[atmos.Assembly, atmos_fhiaims.FHIaims_Assembly, np.ndarray]:
    """Interface function that selects a reader depending on file format.

    Parameters
    ----------
    filepath : Union[str, Path]
        Path of the file to be read.
    format : str, optional
        One of the values (right column) of this dictionary:

            .. literalinclude:: ../atmos/formats/filesio.py
               :language: python
               :lines: 10-20
        
        By default, None.
        When None, we try and determine the format using file's name and suffix (extension).

    Returns
    -------
    Union[atmos.Assembly, List[atmos.Assembly], atmos_fhiaims.FHIaims_Assembly, np.ndarray]
        *assembly* unless files read are numpy data (.np[yz]).

    Raises
    ------
    FileNotFoundError
        If the file was not found at the given path.
    NotImplementedError
        If the reader is yet to be implemented.
    """
    filepath, format = _identify_filepath(filepath, format)
    if not filepath.is_file():
        raise FileNotFoundError(f"Can't find the file: {filepath}")
    parser = {"aimsgeo" : atmos_fhiaims.geofile_parser,
              "aimsout" : atmos_fhiaims.outfile_parser,
              "npy"     : np.load,
              "npz"     : np.load,
              "cube"    : atmos_cube.cube_parser,
              "xyz"     : atmos_xyz.xyz_parser}[format]
    return parser(filepath)

def writer(filepath:Union[str, Path], assembly:atmos.Assembly, quantity:Optional[Union[str, Dict[str, str]]], format:Optional[str], overwrite:Optional[bool], append:Optional[bool]) -> None:
    """Interface function that selects a writer depending on file format.

    Parameters
    ----------
    filepath : Union[str, Path]
        Path of the file to be written.
    assembly : atmos.Assembly
        *assembly* that contains data to be written.
    overwrite : Optional[bool]
        If True, overwrite existing file. If False, do nothing.
        When not declared by user (None), and file exists, raises FileExistsError. 
    append : Optional[bool]
        Only useful when format supports appending.
        If True, append to existing file. If False, do nothing.
        When not declared by user (None), and file exists, raises FileExistsError. 
    quantity : Union[str, Dict[str, str]]
        The quantity that needs to go into the written file. The onus of ensuring 
        correct match between quantity's kind and format is generally on the user.
        When quantity not given, and format supports it, assembly["atoms"] and 
        assembly["positions"] is written out.
        XYZ extended format expects a dictionary as quantity. Any n_scalar or 
        n_vector quantity as well as the color for each of the atoms is 
        supported by the extended XYZ format as written by atmos. The expected
        dictionary should point to the `quantity` as stored in assembly and then
        provide a name for it, to be used in the XYZ file. For example::
            quantity = {"mulliken~charges": "charge",
                        "grimme_d3~forces": "force",
                        "ADD_COLOR": True,
                        }
        Here, the first two entries can be used to write out Mulliken charges 
        and Grimme's D3 forces while the last entry "ADD_COLOR" is a special 
        boolean value that adds the "Color" column to the extended XYZ using
        colors as defined in assembly.colors.
    format : Optional[str]
        The format of the file to be written out. When None, 
        we try and determine the format using file's name and extension.

    Raises
    ------
    TypeError
        If the format requested is incompatible with the quantity selected.
    FileExistsError
        If the user has not used 'overwrite'/'append' but the file already exists.
    ValueError
        If user supplies ``overwrite = True`` as well as ``append = True``.
    NotImplementedError
        If the reader of the given file format is yet to be implemented.
    """
    filepath, format = _identify_filepath(filepath, format)
    # check if the options are all correct
    append_supported = ["xyz"] # list of formats where append is supported
    positions_necessary = ["cube", "xyz", "vaspgeo", "aimsgeo"] # list of formats where positions is necessary
    quantity_necessary  = ["cube", "npy", "npz"] # list of formats where declaring quantity is necessary
    if format == "aimsout":
        raise TypeError("Can't replicate FHIaims output. This feature is unsupported.")
    if quantity is not None:
        if format == "xyz":
            assert isinstance(quantity, dict), f"When writing {format} file, `quantity` argument is expected to be a dictionary."
        else:
            kind = assembly._has_quantity(quantity)
    elif format in quantity_necessary:
        raise TypeError(f"Writing {format} file without a declared 'quantity' is not possible.")
    if format in positions_necessary:
        positions_kind = assembly._has_quantity("positions")
        assert positions_kind, f"When writing {format} file, assembly is expected to have \"positions\" quantity."
        assert positions_kind == "n_vector", f"\"positions\" is expected to be of n_vector kind when writing {format} file."
    if not filepath.suffix.lower() in valid_filekinds.keys() and not filepath.name in valid_filekinds.keys():
        # check if suffix is proper, only triggers if a format has been specified
        for suffix, filekind in valid_filekinds.items():
            if filekind == format:
                filepath = filepath.parent / f"{filepath.name}.{suffix.split('.')[-1]}"
                break
    # check if file exists
    if filepath.exists():
        if overwrite is None and append is None:
            raise FileExistsError("Use one of the following arguments: overwrite, append.")
        if overwrite is not None and append is not None:
            raise ValueError("Use *only* one of the following arguments: overwrite, append.")
        if overwrite is None and append is not None:
            overwrite = False
            assert isinstance(append, bool), "Argument \"append\" must be True or False (i.e. bool)."
        if overwrite is not None and append is None:
            append = False
            assert isinstance(overwrite, bool), "Argument \"overwrite\" must be True or False (i.e. bool)."
        if overwrite is False and append is False:
            print("File exists. Not overwriting/overwriting.") # TODO: Move to UI
            return
        if append and format not in append_supported:
            print(f"append=True is ignored for {format} format. Can only overwrite existing file. Just now: Done nothing.") # TODO: Move to UI
            return
    if format == "xyz":
        atmos_xyz.xyz_writer(filepath, assembly, append, quantity)
    if format == "aimsgeo":
        atmos_fhiaims.geofile_saver(filepath, assembly, quantity)
    if format == "vaspgeo":
        atmos_vasp.poscar_saver(filepath, assembly)
    if format == "cube":
        if not kind == "voxeldat":
            raise TypeError(f"Kind of data: {kind} is incompatible with {format} file.")
        atmos_cube.cube_saver(filepath, assembly, quantity)
    if format in ["npy","npz"]:
        if kind == "scalar":
            np.array(assembly.data.attrs[quantity]).save(filepath)
        else:
            assembly.data[quantity].data.save(filepath)