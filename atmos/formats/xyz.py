from pathlib import Path
from contextlib import suppress
from typing import Dict, Union
import numpy as np
import atmos.assembly as atmos

def xyz_parser(filepath:Path):
    assemblies = []
    raise_warning = False
    with open(filepath, 'r') as stream:
        for line in stream:
            if line.startswith("#"): # Skip commented lines
                continue
            line = line.strip().split()
            if line:
                if len(line) == 1:
                    num_particles = int(line[0])
                    symbols   = []
                    positions = []
                    lattice   = None
                    line = next(stream) # force ignore the next line completely
                    try:
                        if "Lattice" in line:
                            line = line.split("Lattice", maxsplit=1)[1].replace("'","").replace('"',"").replace("=","").split()[:9]
                            lattice = np.array(line, dtype=float).reshape(3,3)
                    except:
                        pass # do not block processing if this fails
                    continue
                else:
                    symbols.append(line[0])
                    positions.append(line[1:4])
                    if not raise_warning and len(line) > 4:
                        raise_warning = True
                    if len(symbols) == num_particles:
                        assembly = atmos.Assembly(num_particles)
                        assembly["atoms"] = symbols
                        assembly["positions"] = positions
                        assembly["positions"].attrs["unit"] = "angstroem"
                        if lattice is not None:
                            assembly.attach("cell","dimensions",lattice)
                        assemblies.append(assembly)
                        del assembly
    if raise_warning:
        print("WARNING: You seem to be reading an extended XYZ file. ATMOS only understands simple XYZ files. Will still attempt to read lattice vectors if available.")
    if len(assemblies):
        if len(assemblies) == 1:
            return assemblies[0]
        else:
            return assemblies
    else:
        raise RuntimeError(f"Could not read any valid XYZ in file: {filepath}")

def xyz_writer(filepath: Path, assembly: atmos.Assembly, append: bool, quantity: Union[None, Dict]):
    add_color = False
    extended_kinds = []
    if quantity is not None:
        for k, v in quantity.items():
            if kind := assembly._has_quantity(k):
                if kind:
                    if kind in ["n_vector", "n_scalar"]:
                        extended_kinds.append(kind)
                    else:
                        raise ValueError(f"xyz writer expects {k} to be n_scalar or n_vector, not {kind}.")
            elif k in ["ADD_COLOR", "ADD_COLORS"]:
                from atmos.helpers.misc import boolean_as_str_input # add some level of resiliency
                add_color = boolean_as_str_input(v)
                if add_color is None:
                    raise TypeError(f"{v}, the value for dict entry {k} is expected to be a boolean.")           
            else:
                raise ValueError(f"Can't identify this quantity: {quantity}. Make sure to .attach() a quantity before operating on it.")
            try:
                v = str(v)
            except TypeError:
                raise TypeError(f"{v}, the value for dict entry {k} cannot be converted to a string.")
        with suppress(KeyError):
            del quantity["ADD_COLORS"]
            del quantity["ADD_COLOR"]            
    
    def formatter_nscalar(value, n:int=None):
        if n is None:
            return f"  {value: 15.10f}"
        return f"  {value:{n}}"
    def formatter_nvector(value, n:int=None):
        if n is None:
            return f"  {value[0]: 15.10f} {value[1]: 15.10f} {value[2]: 15.10f}"
        return f"  {value[0]:.{n}f} {value[1]:.{n}f} {value[2]:.{n}f}"
    def formatter_color(value):
        return formatter_nvector([int(value[i:i+2], 16)/255 for i in (0, 2, 4)], n=3) # from https://stackoverflow.com/questions/29643352/, converting hex to rgb
    
    quans, properties_str = [], []
    
    quans.append([formatter_nscalar(s, n=3) for s in assembly.atoms["symbols"]])
    properties_str.append("species:S:1")

    quans.append([formatter_nvector(p) for p in assembly["positions"].data])
    properties_str.append("pos:R:3")

    if extended_kinds:
        for i, (quantity, label) in enumerate(quantity.items()):
            kind = extended_kinds[i]
            if kind == "n_scalar":
                quans.append([formatter_nscalar(i) for i in assembly[quantity].data])
                properties_str.append(f"{label}:R:1")
            if kind == "n_vector":
                quans.append([formatter_nvector(i) for i in assembly[quantity].data])
                properties_str.append(f"{label}:R:3")

    if add_color:
        properties_str.append("color:R:3")
        quans.append([formatter_color(c) for c in assembly.colors])

    lattice = ""
    if assembly.dimensions is not None:
        for i in range(3):
            lattice += formatter_nvector(assembly.dimensions[i])
        lattice = " ".join(lattice.split())
        lattice = " Lattice=\""+lattice[:-1]+"\" "

    properties_str = str(assembly) + lattice + "Properties=" + ':'.join(properties_str)

    content = "\n" if (append and filepath.is_file()) else ""
    content += f"{len(assembly)}\n"
    content += "" if not (extended_kinds or add_color) else properties_str
    quans = np.column_stack(quans)
    for idx in range(len(assembly)):
        content += "\n"+''.join(quans[idx]).strip()
    
    mode = "a" if append else "w"
    with open(filepath, mode=mode) as stream:
        stream.write(content)