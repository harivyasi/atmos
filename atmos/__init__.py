def _python_check():
    """Function to check python compatibility of ATMOS.

    Using a function to keep the namespace clean.

    Raises
    ------
    ImportError
        If incompatible an version is found.
    """
    import sys
    required_python = (3, 9)
    if sys.version_info < required_python:
        raise ImportError('ATMOS requires Python '+".".join(str(num) for num in required_python))

_python_check()

__all__ = ["Assembly"]

from atmos.assembly import Assembly

