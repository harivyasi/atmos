import re, numpy as np, xarray as xr, pprint, json, copy
from pathlib import Path
from functools import lru_cache
from typing import Dict, List, Optional, Union, Any
from atmos.helpers.variables import el_list as _el_fixed
from atmos.helpers.variables import directchar as _directchar
from atmos.helpers.settings  import el_extra as _el_extra
from atmos.helpers.misc import is_signed_digit as _is_signed_digit

_el_list = _el_fixed + _el_extra

class Assembly:
    """The user facing class that wraps around an xarray.Dataset. 

    When using ATMOS, the user is expected to make instances of this class and 
    use them to perform all actions. An instance of this class is called 
    *assembly* (note: smallcase) in this documentation. 
    
    Each instance holds a varible "*assembly*.data" which is an xarray.Dataset.
    Instances of this class can be obtained in two ways: 

    1. From scratch, using x = Assembly(num_atoms: int), which requires one 
    to know the number of atoms in the assembly. Such a newly created instance 
    holds the following xarray.DataArrays. 
 
        *assembly*.data["atoms"] 

            with rows indexed: "atom_id", 
            filled with <num_atoms> atoms of element 'Xx' (Z=0).

        *assembly*.data["positions"] 

            with rows indexed: "atom_id" 
            and columns indexed: "x", "y", "z",
            filled with default atom positions: nan nan nan. 

        *assembly*.data["_elements"] 

            with rows indexed: "list_id" 
            and columns indexed: "num", "sym", "name", "color", 
            with the default imported from :mod:`atmos.helpers.variables`. 
        
    2. From a saved file that was created in the past, using :func:`load` to 
    load an xarray.Dataset. In-effect, the instance then wraps around the 
    loaded Dataset and provides the same set of functionalities. 

    Private methods of this class are exposed in documentation on purpose. The 
    idea is that to make them readily accessible (when studying documentation) 
    but hide them from user during execution.

    If you find yourself accessing the underlying xarray.Dataset or using one 
    of the private methods often, open an issue. It is indicative of scope for 
    improvement.

    """
    __slots__ = ["data"] # Do not allow user to dynamically create variables, also save memory.
    settings_path = str((Path(__file__).parent / "helpers" / "settings.py"))
    
    def __init__(self, num_atoms:int):
        """Initialize an instance of the Assembly class.

        To use, do ``from atmos import Assembly; x = Assembly(3)`` where 3 is 
        the number of atoms you plan to store and x is the instance of Assembly 
        you create this way.

        Parameters
        ----------
        num_atoms : int
            Number of atoms to have in the instance. Must be a positive integer.
            This can be 0 with the possibility of :func:`insert` but using a
            finite value is speedier.

        Raises
        ------
        ValueError
            When num_atoms is not a positive integer.
        TypeError
            When num_atoms is not an int.
        """
        if isinstance(num_atoms, int):
            if num_atoms < 0:
                raise ValueError("Number of atoms in Assembly cannot be less than 0.")
            data = xr.Dataset()
            data["atoms"] = xr.DataArray(name   = "atoms",
                                         data   = np.full(num_atoms,fill_value="Xx",dtype="U3"),
                                         dims   = ["atom_id"],
                                         coords = {"atom_id": np.arange(num_atoms, dtype=int)},
                                         attrs  = {"kind":"atoms",
                                                   "dtype":"U"})
            data["_elements"] = xr.DataArray(name   = "_elements",                                             
                                             data   = np.array(_el_list, dtype="U20"),
                                             dims   = ["list_id", "details"],
                                             coords = {"list_id": np.arange(len(_el_list), dtype=int),
                                                       "details": ["num","sym","name","color"]},
                                             attrs  = {"kind":"_elements",
                                                           "dtype":"U"})
            self.data = data    
            self.attach("n_vector", "positions")
        else:
            raise TypeError(f"Invalid value type: {type(num_atoms)}, expected: int.")

    def __len__(self) -> int:
        """Tells number of atoms in the *assembly*.

        To access, do ``len(x)``.

        Returns
        -------
        int
            Number of atoms in assembly.
        """
        return self.data.atoms.data.size

    def __repr__(self) -> str:
        """Human readable small summary telling number of atoms.

        To access, do ``print (x)``.

        Returns
        -------
        str
            Summary string.
        """
        if hasattr(self, "uuid") and self.uuid:
            return f"UUID: {self.uuid}. Assembly with {len(self)} atoms."
        if self.dimensions is not None:
            return f"Assembly with {len(self)} atoms enclosed in a cell."
        return f"Assembly with {len(self)} atoms."

    def __setattr__(self, name:str, value:xr.Dataset) -> None:
        """Declared setattr to preserve instances.

        To avoid users overwriting the *assembly*.data with invalid data. 
        But the tests can only go so far.

        Parameters
        ----------
        name : str
            Anything other than "data" wouldn't work (using __slots__).
        value : xarray.Dataset
            One that is compatible with the datastructure created by Assembly.

        Raises
        ------
        ValueError
            If it seems that value is not compatible with datastructure 
            created by Assembly.
        TypeError
            If the value supplied is not not an xarray.Dataset.
        """
        if name == "data":
            if isinstance(value, xr.Dataset):
                if not "atoms" in value.data_vars.keys():
                    raise ValueError("Supplied xarray.Dataset doesn't contain an `atoms` xr.Dataarray. Make sure it is compatible with Assembly.")
            else:
                raise TypeError("x.data can only be replaced by another xarray.Dataset.")
        super().__setattr__(name, value)

    def __delattr__(self, name:str) -> None:
        """Prevents user from deleting *assembly*.data.

        Parameters
        ----------
        name : str
            Anything other than "data" wouldn't work (using __slots__).

        Raises
        ------
        ValueError
            Tells user that they can't delete *assembly*.data.
        """
        if name == "data":
            raise ValueError("x.data can not be deleted.")
        super().__delattr__(name)

    def __setitem__(self, idx:Union[int, slice, str], value:Optional[Union[np.ndarray, list, int, float, str, bool]]) -> None:
        """To set values for quantities that are already attached.

        See :func:`_set_value` and :func:`_set_atom` for more details.

        Parameters
        ----------
        idx : Union[int, slice, str]
            Using int or slice operates on the "atoms" data. All data (including 
            "atoms") is accessible using free-form text (str) given in quotes. See
            :func:`_process_idx` for more.

        value : Optional[Union[np.ndarray, list, int, float, str, bool]]
            The value that has been supplied.
            When value is None, uses the nan value depending on dtype.
        """
        kind, quantity, selection, direction = self._process_idx(idx)
        if kind == "atoms":
            self._set_atom(value, selection)
        else:
            self._set_value(kind, quantity, value, dtype=None, selection=selection, direction=direction)
    
    def __lshift__(self, command:str) -> None: # process commands
        """Perform operations on *assembly*.

        Use the << operator to use this function. For example you can do::

            my_assembly << move 3
        
        where "move" is the `command` and "3" is the `magnitude` of "move".

        See `func`:atmos.helpers.operations.interpret for more.
        """
        if not isinstance(command, str):
            command = str(command)
        from atmos.helpers.operations import interpret
        interpret(self, command)

    def __getitem__(self, idx:Union[int, slice, str]) -> Union[xr.DataArray, int, float, str, bool]:
        """To get values of quantities that are already attached.
        
        Parameters
        ----------
        idx : Union[int, slice, str]
            Using int or slice operates on the "atoms" data. All data (including 
            "atoms") is accessible using free-form text (str) given in quotes. See
            :func:`_process_idx` for more.

        Returns
        -------
        Union[xr.DataArray, int, float, str, bool]
            If data is of kind = "scalar", that is returned as it is was
            supplied: most likely an int, float, str or bool.
            Otherwise, xr.DataArray with the requiste data. Underlying numpy 
            array can be access using .data on returned object.
        """
        kind, quantity, selection, direction = self._process_idx(idx)
        if kind == "scalar":
            return self.data.attrs[quantity]
        else:
            if direction is not None and selection is not None:
                return self.data[quantity].loc[selection, direction]
            elif direction is not None and selection is None:
                if kind == "vector":
                    return self.data[quantity].loc[direction]
                return self.data[quantity].loc[:, direction]
            elif direction is None and selection is not None:
                return self.data[quantity].loc[selection]
            else:
                return self.data[quantity]

    def __delitem__(self, idx:Union[int, slice, str]) -> None:
        """To get values of quantities that are already attached.

        Deleting "atoms" deletes all data that is associated with an atom. 
        Generally speaking, all "atoms", "n_scalar", "n_vector" kind of data 
        is deleted. Data of kinds "scalar", "vector", "cell", "voxeldat", 
        "arbit" are preserved.

        Parameters
        ----------
        idx : Union[int, slice, str]
            Using int or slice operates on the "atoms" data. All data (including 
            "atoms") is accessible using free-form text (str) given in quotes. See
            :func:`_process_idx` for more.
        """        
        kind, quantity, selection, direction = self._process_idx(idx)
        if kind == "atoms":
            if selection:
                print("Warning: deleting 'atoms' and corresponding data in all quantities. Make sure data is consistent.") # TODO: Move to UI
                new_ds = self.data.drop_sel(atom_id=selection)
                len_new_ds = new_ds.atoms.data.size
                self.data = new_ds.assign_coords(atom_id=np.arange(len_new_ds, dtype=int))
            else:
                to_save = []
                for quantity in self.data.data_vars.keys():
                    if not "atom_id" in self.data[quantity].dims:
                        to_save.append(quantity)
                saved_da = {}
                for quantity in to_save:
                    saved_da[quantity] = self.data[quantity]
                saved_scalars = self.data.attrs
                new_assembly = self.load()
                new_assembly.data.attrs.update(saved_scalars)
                for quantity in to_save:
                    new_assembly.data[quantity] = saved_da[quantity]
                self.data = new_assembly.data
                print("Warning: Deleted all x[\"atoms\"]. len(x) is now zero. All atom-associated data has also been deleted.")
        elif kind == "scalar":
            del self.data.attrs[quantity]
        else:
            if selection or direction:
                self._set_value(kind, quantity, None, dtype=None, selection=selection, direction=direction) # replace only a subset of data but setting it to 'None'
            else:
                assert (selection, direction) == (None, None)
                new_ds = self.data.drop_vars(quantity) # remove complete DAs
                self.data = new_ds

    def _has_quantity(self, quantity:str) -> str:
        """Check and determine if *assembly* has a particular quantity.

        Parameters
        ----------
        quantity : str
            Quantity whose existence has to be checked.

        Returns
        -------
        str
            Any of the kinds ["scalar", ..., "n_vector", ..., "cell"] if `quantity` exists,
            else "". See :func:`attach` for a list of valid kinds.
        """
        assert isinstance(quantity, str), "_has_quantity expects quantity to be of kind: str."
        if quantity in self.data.data_vars.keys():
            return self.data[quantity].attrs["kind"]
        elif quantity in self.data.attrs.keys():
            return "scalar"
        else:
            return ""
    
    def info(self, quantity:str = None) -> List[str]:
        """Prints info attached as attributes (dictionary) to the quantity.

        Parameters
        ----------
        quantity : str, optional
            Quantity whose attributes are to be printed. If quantity is scalar,
            the quantity itself is printed.

        Returns
        -------
        quantities : List[str]
            A list of all matching quantities.

        Raises
        ------
        ValueError
            If an unknown quantity is given.
        """
        if quantity is None:
            quantities = self.quantities
        else:
            assert isinstance(quantity, str), "x.info() expects quantity to be of type: str."
            quantity = quantity.strip().lower()
            quantities = []
            for quan in self.quantities:
                if quan == quantity:
                    quantities.append(quan)
                if quan.lstrip(quantity).startswith("~"):
                    quantities.append(quan)
            if not quantities:
                raise ValueError(f"Can't identify this quantity: {quantity}. Make sure to .attach() a quantity before operating on it.")
        for quantity in quantities:
            if (kind := self._has_quantity(quantity)) and kind == "scalar":
                print(f"Scalar: '{quantity}' ->")
                pprint.pprint(self.data.attrs[quantity], depth = 3)
                print()
            elif kind:
                include_dims = False
                for dim in list(self.data[quantity].dims):
                    if not dim.startswith("arbit"):
                        include_dims = True
                if include_dims:
                    print(f"DatArr: '{quantity}' with shape: {self.data[quantity].shape} and having: {self.data[quantity].dims} ->")
                else:
                    print(f"DatArr: '{quantity}' with shape: {self.data[quantity].shape} ->")
                pprint.pprint(self.data[quantity].attrs, depth = 3)
                print()
            else:
                raise ValueError(f"Can't identify this quantity: {quantity}. Make sure to .attach() a quantity before operating on it.")
        return quantities

    def _test_dtype(self, dtype:str) -> str:
        """A function to understand and limit dtypes that get stored in *assembly*.

        Parameters
        ----------
        dtype : Union[str]
            Ideally, one of "str", "int", "float32", "float", "bool". 
            There is also scope for handling str(np.dtype).

        Returns
        -------
        str
            One of these: 'U', 'f', 'd', 'l', '?' 
            standing for strings, float32s, float64s, ints and bools respectively.

        Raises
        ------
        ValueError
            If it can't understand the given dtype.
        """
        valid_dtypes = {"str":     'u', # will capitalize in the end
                        "string":  'u',
                        "float16": 'f', # Not supporting half-floats
                        "float32": 'f',
                        "float64": 'd',
                        "float":   'd',
                        "int32":   'l', # Not supporting short ints
                        "int64":   'l', 
                        "int":     'l', # it's an L, not a One
                        "bool":    '?',
                        "boolean": '?'}
        try:
            dtype = str(dtype)
            # to handle edge cases where user sends in an np.dtype or sends it in caps
        except DeprecationWarning:
            pass # don't want to bother user with numpy deprecation warnings #TODO
        dtype = dtype.lower()
        if dtype in valid_dtypes.keys():
            dtype = valid_dtypes[dtype]
        elif dtype in valid_dtypes.values():
            pass
        # elifs below take care of numpy dtypes
        elif dtype.startswith("<u") or "str" in dtype or "unicode" in dtype:
            dtype = "u"
        elif "bool" in dtype or "boolean" in dtype:
            dtype = "?"
        elif "int" in dtype:
            dtype = "l"
        elif "float16" in dtype or "float32" in dtype:
            dtype = "f"
        elif "float" in dtype or "float64" in dtype:
            dtype = "d"
        else:
            if str(dtype) == "object":
                raise ValueError(f"Can't validate the dtype: {dtype}. Are you creating an ndarray from ragged nested sequences with different lengths or shapes. That is not supported!")
            else:
                raise ValueError(f"Can't validate the dtype: {dtype}")
        if dtype == "u":
            dtype = "U"
        return dtype

    def _process_idx(self, idx:Union[int, slice, str]) -> tuple[str, str, Optional[list[int]], Optional[str]]:
        """Process free-form text used for selecting parts of *assembly*.

        The function is called for any call to *assembly* with square brackets e.g. 

            - assembly[5]               # the 6th atom 
            - assembly["7"]             # the 8th atom 
            - assembly[7:10]            # the 8th to 10th atom 
            - assembly["7:10"]          # the 8th to 10th atom 
            - assembly["charges 3"]     # charge of the 4th atom 
            - assembly["charges 3:10"]  # charges on atoms 4th to 10th
            - assembly["forces y 2"]    # y component of forces acting on the 3rd atom 
            - assembly["positions z 5"] # z component of positons of the 6th atom 

        Parameters
        ----------
        idx : Union[int, slice, str]
            Using int or slice operates on the "atoms" data. All data (including 
            "atoms") is accessible using free-form text (str) given in quotes.

        Returns
        -------
        kind : str
        quantity : str
        selection : list[int] or None
            selection of index/indices; all indices if None
        direction : str or None
            direction ("x", "y" or "z"); all directions if None

        Raises
        ------
        NotImplementedError
            If user tries to use features that are not implemented yet.
        IndexError
            If the index is out of range or not valid with the given quantity.
        ValueError
            If the values given in free-form text could not be comprehended.
        """
        if isinstance(idx, slice): # convert to str to handle using list[int] indexing
            str_idx = "::" if idx.stop is None else f":{str(idx.stop)}:"
            if idx.start:
                str_idx = str(idx.start)+str_idx
            if idx.step:
                str_idx = str_idx+str(idx.step)
            idx = str_idx
        if isinstance(idx, int):
            idx = str(idx)       
        assert isinstance(idx, str), "idx must be of type: str, int or slice."
        idx = str(idx).lower().strip()
        if "==" in idx or "," in idx:
            raise NotImplementedError("We plan to include comparison-enabled selection later. It will use , and ==.")
        sep = None # when idx is a free-form space separated text, other implementations are # TODO after match is available in Python 3.10.
        idx = idx.split(sep)
        kind, quantity, selection, direction = None, None, None, None
        for substring in idx:
            if _is_signed_digit(substring):
                if selection is not None:
                    raise ValueError(f"More than one selection identified: {selection} and {substring}")
                selection = int(substring)
                if not -len(self) <= selection < len(self):
                    raise IndexError(f"Index out of range -{len(self)}..0..{len(self)-1}: {idx}")
                if selection < 0:
                    selection = len(self) + selection
                selection = [selection] # convert it into list so that we only ever deal with list of indices
            elif ":" in substring: # a slice has been asked
                if selection is not None:
                    raise ValueError(f"More than one selection identified: {selection} and {substring}")
                for char in substring: # test the code for input (using eval carefully!)
                    if not (char in ":-" or _is_signed_digit(char)):
                        raise ValueError(f"Can't identify this slicing: {substring}.")
                selection = list(range(len(self)))
                selection = eval(f"selection[{substring}]")                    
            elif substring in _directchar.keys():
                if direction is not None:
                    raise ValueError(f"More than one direction identified: {direction} and {substring}")
                direction = _directchar[substring]
            else:
                if kind := self._has_quantity(substring): 
                    if quantity is not None:
                        raise ValueError(f"More than one quantity identified: {quantity} and {substring}")
                    quantity = substring
                    if quantity == "_elements":
                        raise ValueError("No operations are allowed on _elements. Use x.colors or x.data (with care) to access it.")
                else:
                    raise ValueError(f"Can't identify this quantity: {substring}. Make sure to .attach() a quantity before operating on it.")
        if kind is None: # assuming some defaults
            if direction is None:
                kind, quantity = "atoms", "atoms"
            else:
                kind, quantity = "n_vector", "positions"
        # validate if the combination makes sense:
        not_allowed = {"atoms"   : [direction],
                       "scalar"  : [selection, direction],
                       "vector"  : [selection],
                       "n_scalar": [direction],
                       "n_vector": [],
                       "voxeldat": [selection, direction],
                       "cell"    : [selection, direction],
                       "arbit"   : [selection, direction]}
        for coords in not_allowed[kind]:
            if coords is not None:
                raise IndexError(f"Cannot select {coords} in {quantity} ({kind}).")
        return kind, quantity, selection, direction

    def _identify_atom(self, value:Union[str, int]) -> tuple[int, str, str, str]:
        """Identify the element of an atom depending on the supplied value.

        Parameters
        ----------
        value : Union[str, int]
            When str, first try and look for symbol, then for name.
            When int, assume it to be atomic number. Z: 0 to 102 are supported.
            Z = 0, "Xx", "Empty" serves as a nan value in case of "atoms" kind.
            The look-up table is copied from :mod:`atmos.helpers.variables` 
            when the *assembly* is created. Thereafter, it is stored in the 
            *assembly*.

        Returns
        -------
        tuple[int, str, str, str]
            - int: Atomic number of the atom (cannot convey information about duplicate species) 
            - str: Element symbol for the atom
            - str: English name of the atom
            - str: Color of the atom

        Raises
        ------
        KeyError
            If Z is out of range. 0 <= Z <= 102 is the supported range.
        ValueError
            If the element can't be uniquely identified, or is an 
        """
        value = str(value)
        el_da = self.data["_elements"]
        if value.isnumeric():
            # If (atomic) number is given, custom species would be not be considered.
            if 0 <= int(value) <= 102:
                list_id = el_da[:102].loc[el_da[:102].loc[:,'num']==value].list_id
            else:
                raise KeyError("Only elements with 0 <= Z <= 102 are supported.")
        else:
            value = value.title()
            list_id = el_da.loc[el_da.loc[:,'sym']==value].list_id
            if len(list_id) == 0:
                list_id = el_da.loc[el_da.loc[:,'name']==value].list_id
        if len(list_id) == 1:
            pass
        elif len(list_id) > 1:
            raise ValueError(f"Given atom: {value} matches more than one atomic symbols.")
        else:
            raise ValueError(f"Can't identify atom: {value}")
        data = list(el_da.loc[list_id].data.flatten())
        z, sym, name, color = int(data[0]), *data[1:]
        return z, sym, name, color
    
    def _set_atom(self, value:Union[int,str,list[int],list[str]], selection: Optional[list[int]] = None) -> None:
        """Set value for given atoms.

        Parameters
        ----------
        value : Union[int,str,list[int],list[str]]
            Sent to :func:`_identify_atom` to resolve.
            When a list, sent one by one.
            If None, set as nan for "atoms" kind.
        selection : Optional[list[int]], optional
            Value(s) to assign. Expected value(s) are Z(s), symbol(s) or name(s).
            If None, all atoms are reassigned.

        Raises
        ------
        ValueError
            If there is a length mismatch between data placeholder and supplied data.
        """
        expected_length = len(self) if selection is None else len(selection)
        if value is None:
            data = np.full(len(expected_length), fill_value="Xx", dtype="U3")
        else:
            data = np.array(value, dtype="U3").flatten()
            for idx, identifier in enumerate(data): # make sure they are atom type
                data[idx] = self._identify_atom(identifier)[1]
        if len(data) != expected_length:
            raise ValueError(f"Please make sure that data has length: {expected_length}.")
        if selection is None:
            self.data["atoms"] = xr.DataArray(name   = "atoms",
                                              data   = np.array(data, dtype="U3"),
                                              dims   = ["atom_id"],
                                              attrs  = {"kind":"atoms",
                                                        "dtype":"U"})
        else:
            self.data["atoms"].loc[selection] = data
    
    def _set_value(self, kind:str, quantity:str, value:Optional[Any], *, dtype:Optional[str] = None, selection:Optional[list[int]] = None, direction:Optional[str] = None) -> None:
        """Used to intitialize as well as set values in *assembly*.data.

        Called from :func:`attach` to initialize, :func:`__setitem__` to change 
        existing values and from :func:`__delitem__` to set deleted values to 
        nan. 
        
        Parameters
        ----------
        kind : str
            One of the valid kinds, as listed in :func:`attach`.
        quantity : str
            The name of quantity that to be operated upon.
        value : Optional[Any]
            Value that needs to be stored. We do check for shape but ultimate 
            test for data compatibility (shape and dtype) happens in numpy.
            If None, a dtype-dependent nan value is stored. 
        dtype : Optional[str]
            Keyword argument. One of 'U', 'f', 'd', 'l', and '?'.data. If None 
            is passed, it must already be stored with the DataArray as an attrs 
            entry.
        selection : Optional[list[int]], optional
            List of indices to perform the operation on. By default None, 
            implying perform on everything (in the given direction)!
        direction : Optional[str], optional
            The column of 'vector' or 'n_vector' to perform operation on. 
            By default None, implying perform on all directions (for the 
            selected atoms).

        Raises
        ------
        ValueError
            If the value that is being set doesn't have correct shape. Shape is
            determined by the choice of kind, selection and direction.
        """
        if kind == "scalar":
            self.data.attrs[quantity] = value
            return
        # prepare data for other types
        if dtype is None: # in case, data must already exist
            try:
                dtype = self.data[quantity].attrs["dtype"]
            except KeyError:
                assert False, "dtype must be stored for all DataArrays at initialization."
        if dtype == "U":
            dtype = "U1024" # artficial limit on length of strings, has to be done to store it in numpy
        nan = {'U1024': "",
                   'f': np.nan,
                   'd': np.nan,
                   'l': 0,
                   '?': False}
        assert dtype in nan.keys(), "Unknow dtype must never be encountered!"
        nan = nan[dtype]
        if selection is not None:
            if len(selection) == 0: # nothing selected by the list, do nothing: can happen if slicing returns an empty list
                return
        # for vector data; initialize with [nan, nan, nan] if no value is given                
        if kind == "vector":
            if value is None:
                value = nan if direction is not None else np.array([nan, nan, nan], dtype=dtype)
            data = np.array(value, dtype=dtype).flatten()
            if direction is None:
                if len(data) != 3:
                    raise ValueError("Please make sure that data of length: 3.")
                self.data[quantity] = xr.DataArray(name   = quantity,
                                                    data   = data, 
                                                    dims   = ["direction"], 
                                                    coords = {"direction":["x", "y", "z"]},
                                                    attrs  = {"kind": "vector", "dtype": dtype})
            else:
                if len(value) == 1:
                    self.data[quantity].loc[direction] = value[0]
                else:
                    raise ValueError("Please make sure that data has length: 1")
            return
        # for n_scalar data; initialize with [nan]*number_of_atoms if no value is given
        if kind == "n_scalar":
            expected_length = len(self) if selection is None else len(selection)
            if value is None:
                data = np.full(expected_length, nan, dtype=dtype)
            else:
                data = np.array(value, dtype=dtype).flatten()
            if len(data) != expected_length:
                raise ValueError(f"Please make sure that data has length: {expected_length}.")
            if selection is None:
                self.data[quantity] = xr.DataArray(name  = quantity,
                                                   data  = data,
                                                   dims  = ["atom_id"],
                                                   attrs = {"kind": "n_scalar", "dtype": dtype})
                                                   # coords are coming in from the 'atoms' DataArray
            else:
                self.data[quantity].loc[selection] = data
            return
        # for n_vector data; initialize with [nan, nan, nan]*number_of_atoms if no value is given
        if kind == "n_vector":
            expected_length = len(self) if selection is None else len(selection)
            expected_directions = 3 if direction is None else 1
            if value is None:
                data = np.full((expected_length,expected_directions), nan, dtype=dtype)
            else:
                data = np.array(value, dtype=dtype)
            if data.ndim != 2:
                data = data.flatten()
                if len(data) % expected_directions:
                    raise ValueError(f"Please make sure that n-vector has a shape of {expected_length,expected_directions}.")
                else:
                    data = data.reshape((len(data)//expected_directions, expected_directions))
            if data.shape != (expected_length,expected_directions):
                raise ValueError(f"Please make sure that n-vector has a shape of {expected_length},3.")
            if direction is not None:
                data = data.flatten() # because .loc returns an array with ndim = 1
            if direction is None and selection is None:
                self.data[quantity] = xr.DataArray(name   = quantity,
                                                   data   = data,
                                                   dims   = ["atom_id", "direction"],
                                                   coords = {"direction":["x", "y", "z"]},
                                                   attrs  = {"kind": "n_vector", "dtype": dtype})
                                                   # coords for "atom_id" coming in from the 'atoms' DataArray
            elif selection is None and direction is not None:
                self.data[quantity].loc[:, direction] =  data
            elif selection is not None and direction is None:
                self.data[quantity].loc[selection] = data
            elif selection is not None and direction is not None:
                self.data[quantity].loc[selection, direction] = data
            else:
                assert False, "This should never happen!"
            return
        # for voxel data
        if kind == "voxeldat":
            if value is None:
                raise ValueError("You must initialize voxel-data with a value.")
            if dtype == "U1024" and isinstance(value, Path):
                if value.suffix in [".cube",".cub"]:
                    import atmos.formats.cube as atmos_cube
                    cube_assembly = atmos_cube.cube_parser(value, quantity=quantity, attach_voxeldat=True)
                    data = cube_assembly[quantity].data
                    dtype = self._test_dtype(data.dtype)
                    cube_assembly.data = cube_assembly.data.drop_vars(quantity)
                    self.data = self.data.merge(cube_assembly.data, compat="override", combine_attrs="no_conflicts") # Do not replace existing data
                elif value.suffix in [".npy",".npz"]:
                    import atmos.formats.filesio as atmos_filesio
                    data = atmos_filesio.reader(value, format="npy")
                    dtype = self._test_dtype(data.dtype)
                else:
                    raise ValueError(f"Can't read file of type: {value.suffix}.")
            else:
                data = np.array(value, dtype=dtype)
            if (np.array(data)).ndim != 3:
                raise ValueError("Voxel-data must be three dimensional.")
            shape = data.shape
            dims =   []
            coords = {}
            for length in shape:
                dims.append(f"cube_{length}")
                coords[f"cube_{length}"] = list(range(length))
            self.data[quantity] = xr.DataArray(name  = quantity,
                                               data  = data,
                                               coords= coords,
                                               dims  = dims,
                                               attrs = {"kind": "voxeldat", "dtype": dtype})
            return
        # for cell dimensions
        if kind == "cell":
            fill_value = nan if value is None else 0
            value = np.array([nan, nan, nan], dtype=dtype) if value is None else np.array(value, dtype=dtype).flatten()
            if len(value) == 3:
                data = np.full((3,3), fill_value, dtype=dtype)
                np.fill_diagonal(data, value)
            elif len(value) == 9:
                data = value.reshape((3,3))
            else:
                raise ValueError(f"Please make sure that cell has a shape of 3, (3,3) or 9.")
            self.data[quantity] = xr.DataArray(name   = quantity,
                                               data   = data,
                                               dims   = ["direction","span"],
                                               coords = {"direction":["x","y","z"], "span":[0, 1, 2]},
                                               attrs  = {"kind": "cell", "dtype": dtype})
            return
        # for arbitrary numpy data
        if kind == "arbit":
            if value is None:
                raise ValueError("You must initialize arbitrary data with a value.")
            elif dtype == "U1024" and isinstance(value, Path):
                import atmos.formats.filesio as atmos_filesio
                if value.suffix in [".npy",".npz"]:
                    data = atmos_filesio.reader(value, format="npy")
                    dtype = self._test_dtype(data.dtype)
                else:
                    raise ValueError(f"Can't read file of type: {value.suffix}.")
            else:
                data = np.array(value, dtype=dtype)
            if data.ndim:
                shape = data.shape
                dims =   []
                coords = {}
                for length in shape:
                    dims.append(f"arbit_{length}")
                    coords[f"arbit_{length}"] = list(range(length))
            else:
                dims   = ["arbit"]
                coords = {"arbit":0}
            self.data[quantity] = xr.DataArray(name  = quantity,
                                               data  = data,
                                               dims  = dims,
                                               coords= coords,
                                               attrs = {"kind": "arbit", "dtype": dtype})
            return

    def attach(self, kind: str, quantity: str, value:Union[int, float, bool, str, np.ndarray, Path, None] = None, dtype:Optional[str] = None, unit:str = None) -> None:
        """Attach data to this instance of *assembly*.

        Scalar data is attached as an attribute to *assembly*.data.
        All other data is converted to an xarray.DataArray and added to the *assembly*.data Dataset.

        Parameters
        ----------
        kind : str 
            One of these:
             
             - "scalar" is for scalar quanities associated with the complete assembly i.e. that have no components, e.g. total charge. 
             - "vector" is for vector quanities associated with the complete assembly i.e. that have 3 directional components, e.g. total force. 
             - "n_scalar" is for scalar quanities associated each atom, e.g. charge on each atom. 
             - "n_vector" is for vector quanities associated each atom, e.g. force acting on each atom. 
             - "voxeldat" is for scalar quanities on a defined 3D mesh, mainly a convenience feature to store cube file. 
             - "cell" is for storing the 3 lattice vectors that form a cell, mainly a convenience feature to store *dimensions* of cell. 
             - "arbit" is for storing/retrieving any arbitrary np.ndarray with assembly. No operations are allowed on it. 
        quantity : str 
            A unique name for the attached data. Allowed characters for quanity name are a-z, 0-9 and _. 
            No captital case. The name must have at least 2 characters. ~ is also allowed but has special use case. 
        value : Optional[Union[int, float, bool, str, numpy.ndarray, Path]] 
            Scalar / Array-like data to be stored with the given name. 
            When not given, a default is chosen depending on dtype (nan for floats, 0 for ints, "" for strings and False for bools.) 
            Path only when loading .cube and .np[yz]
        dtype : Optional[str] 
            One of the following: "str", "int", "float32", "float", "bool". "float" is same as "float64".
            When given, value (unless Scalar) will be cast to the selected dtype.
            When not given, it tries to infer from value. If value is not given, it is float.
        unit :  Optional[str]
            Unit that you want to store in the attrs dictionary of the DataArray.

        Raises
        ------
        ValueError
            If any of the passed arguments is incorrect.
        """
        valid_kinds = ["scalar", "vector", "n_scalar", "n_vector", "voxeldat", "cell", "arbit"]
        allowed_quantity_names = re.compile(r"[a-z0-9_~]")
        kind = kind.lower().strip()
        if not kind in valid_kinds:
            raise ValueError(f"Can't recognize the kind: {kind} given.")
        quantity = quantity.strip() # not lowering the case of quantity: wrong usage should inform user.
        if not len(quantity) > 1:
            raise ValueError(f"Name of quantity must be 2 characters or more.")
        if "".join(allowed_quantity_names.findall(quantity)) != quantity:
            raise ValueError("Allowed characters in quanity name are a-z, 0-9 and _.")
        if self._has_quantity(quantity):
            raise ValueError(f"Quantity called {quantity} already attached.")
        if kind == "scalar":
            self._set_value("scalar", quantity, value)
        else:
            if dtype is None and value is None:
                value = None # will be turned into np.nans of appropriate shape by _set_value()
                dtype = 'd'  # float64
            elif dtype is None and value is not None:
                if kind in ["voxeldat","arbit"] and isinstance(value, (str,Path)):
                    dtype = 'U' # so that npys and cubes can be opened.
                    value = Path(value)
                else:
                    value = np.array(value)
                    dtype = self._test_dtype(str(value.dtype)) # infer dtype from array
            elif dtype is not None and value is None:
                value = None # will be turned into np.nans of appropriate shape by _set_value()
                dtype = self._test_dtype(dtype)
            else: # i.e. both dtype and value have been given
                dtype = self._test_dtype(dtype)
                value = np.array(value, dtype=dtype) # cast as told by the user
            self._set_value(kind, quantity, value, dtype=dtype)
        assert self._has_quantity(quantity), f"{quantity} not attached to assembly."
        if unit is not None:
            if kind == "scalar":
                raise ValueError(f"{kind} kind does not support unit.")
            if not isinstance(unit, str):
                raise ValueError(f"unit is supposed to be of type 'str', not {type(unit)}.")
            self[quantity].attrs['unit'] = unit
    
    def detach(self, quantity:str) -> None:
        """Detach a given quantity. All information about it is lost.

        "_elements" cannot be detached. For "voxeldat", associated quantities 
        are also lost. See :func:`__delitem__` for more details.

        Parameters
        ----------
        quantity : str
            Name of the quantity to detach.
        """
        self.__delitem__(quantity)

    def insert(self) -> None:
        raise NotImplementedError("Yet to be done") # TO DO: add list-like insert

    def remove(self) -> None:
        raise NotImplementedError("Yet to be done") # TO DO: add list-like remove
    
    @lru_cache(maxsize=10)
    def _cached_get_atoms(self, atomic_data:tuple[str]) -> tuple[np.ndarray, np.ndarray]:
        """Cached function to aid :func:`atoms`.

        The operations are heavy as very atom must be identified in the 
        *assembly*.data["_elements"] array. Test shows it takes 0.5 seconds per 
        100 atoms. On the other hand, once declared, we don't expect atoms to 
        change much (current usage model). Therefore an ideal case for caching. 

        Parameters
        ----------
        atomic_data : tuple[str]
            A tuple of atomic symbols of all atoms in *assembly*.

        Returns
        -------
        Same as :func:`atoms` and :func:`colors`
        """
        atoms, colors = [], []
        for i in range(len(atomic_data)):
            atom = self._identify_atom(atomic_data[i])
            atoms.append(atom[:3])
            colors.append(atom[3])
        atoms = np.array(atoms, dtype=[("Zs", int), ("symbols", "U3"), ("names", "U20")])
        colors = np.array(colors, dtype="U6")
        return atoms, colors

    @property
    def atoms(self) -> np.ndarray:
        """Attribute to look up persistent data about atoms.

        Indexing is same as rest of *assembly*. It's a wrapper 
        around :func:`_cached_get_atoms`. 

        Returns
        -------
        np.ndarray with dtype = [("Zs", int), ("symbols", "U3"), ("names", "U20")]
            This can then be accessed as 

              - *assembly*.atoms["Zs"]: Atomic numbers of all the atoms (cannot convey information about duplicate species). 
              - *assembly*.atoms["symbols"]: Element symbols for all the atoms. 
              - *assembly*.atoms["names"]: English names of all the atoms.  
        """
        atomic_data = tuple(self.data["atoms"].data) # tuple-d to make it hashable
        atoms, colors = self._cached_get_atoms(atomic_data)
        return atoms
    
    @property
    def dimensions(self) -> Optional[np.ndarray]:
        """Attribute to access cell dimensions and check periodic boundary condition.

        Convinience method.

        Returns
        -------
            np.ndarray: dimensions of the cell in a 3x3 numpy array or
            None: if periodic boundary condition is not used.
        """
        pbc = self._has_quantity("dimensions")
        if pbc == "cell":
            return self.data.dimensions.data
        else:
            return None
            
    @dimensions.setter
    def dimensions(self, value) -> None:
        raise AttributeError("Use x[\"dimensions\"] = value to set dimensions.")

    @dimensions.deleter
    def dimensions(self):
        raise AttributeError("Use del x[\"dimensions\"] to delete dimensions.")
    
    @property
    def colors(self):
        """Attribute to access color information of each atom.

        Setting color has expected form of::

            assembly.colors = ('Fe','E06633') # to reassign color for Iron.

        Deleting color resets the value to that imported from :mod:`atmos.helpers.variables`.
        
        Returns
        -------
        list[str]
            Hex values of color for all the atoms in *assembly*.
        """
        atomic_data = tuple(self.data["atoms"].data) # tuple-d to make it hashable
        atoms, colors = self._cached_get_atoms(atomic_data)
        return list(colors)

    @colors.setter
    def colors(self, value:tuple[(str, str)]) -> None:
        expected_format = "Expected format is `x.colors = ('Fe','E06633')` to set color."
        if not isinstance(value, tuple):
            raise ValueError(expected_format)
        else:
            sym, color = value
        if not isinstance(sym, str) or not isinstance(color, str):
            raise ValueError(expected_format)
        color = color if color.startswith('#') else '#'+color
        sym = self._identify_atom(sym)[1] # confirm that the symbol is valid
        matches_pattern = re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', color) # from https://stackoverflow.com/questions/30241375/
        if not matches_pattern:
            raise ValueError(expected_format+" Check the color hex.")
        color = color[1:] # remove the #
        list_id = self.data["_elements"][:102].loc[self.data["_elements"][:102].loc[:,'sym']==sym].list_id
        self.data["_elements"].loc[list_id, 'color'] = color
        self._cached_get_atoms.cache_clear()

    @colors.deleter
    def colors(self) -> None:
        data = np.array(_el_list, dtype="U20")
        self.data["_elements"].loc[:, 'color'] = data[:,3]
        self._cached_get_atoms.cache_clear()

    @property
    def quantities(self) -> list[str]:
        """Attribute to get a list of associated quanties.

        Looks up both: scalars as well as all others. Doesn't show "_elements".

        Returns
        -------
        list[str]
            A sorted list of all associated quanties.
        """
        quantities = sorted(list(self.data.data_vars.keys())+list(self.data.attrs.keys()))
        quantities.remove("_elements")
        return quantities

    def save(self, path:Union[Path, str]):
        """Save *assembly*. Recommended extension is '.nc'.

        Parameters
        ----------
        path : Union[Path, str]
            Path of where to save the file.
        """
        encoding = {}
        attrs = copy.deepcopy(self.data.attrs)
        self.data.attrs.clear()
        for quantity in self.quantities:
            # no scalars should be encountered
            kind = self._has_quantity(quantity)
            if self.data[quantity].attrs["dtype"] == '?':
                self.data[quantity].data = self.data[quantity].data.astype(int)
            encoding[quantity] = {}
            if kind in ["voxeldat","arbit"]:
                encoding[quantity].update({"zlib": True, "complevel": 9})
        self.data.attrs["json_attrs"] = json.dumps(attrs)
        self.data.to_netcdf(path, format="NETCDF4", engine="h5netcdf", encoding=encoding, invalid_netcdf=False)
        # restore previous DataSet
        self.data.attrs.clear()
        self.data.attrs.update(attrs)
        for quantity in self.data.data_vars.keys():
            if self.data[quantity].attrs["dtype"] == '?':
                self.data[quantity].data = self.data[quantity].data.astype(bool)
    
    @classmethod
    def load(cls, value:Union[str, Path, xr.Dataset] = None):
        """Load a previously saved *assembly* or create a new empty one.

        The function *does not* overwrite data of the *assembly* that was used 
        to call it. For example::
            
            from atoms import Assembly
            my_assembly = Assembly.load("/previously_saved.nc")
        
        loads the previously saved *assembly* but::

            my_assembly.load("/another_saved.nc")

        would not overwrite information in my_assembly. To do the overwrite, you 
        have to assign the returned value to a variable::

            another_assembly = my_assembly.load("/another_saved.nc")
            my_assembly = my_assembly.load("/another_saved.nc") # to overwrite

        Parameters
        ----------
        value : [Union[str, Path, xr.Dataset]
            If None: It means a new *assembly.data* object of len 0 is asked for.
            If Path or str: Path of previously saved *assembly* (.nc) file.
            If xr.Dataset: An *assembly.data* object is assumed.

        Returns
        -------
        Assembly
            *assembly* instance that was loaded/created.

        Raises
        ------
        FileNotFoundError
            If specified file was not found.
        """
        if value is None:
            assembly = cls(0)
            return assembly
        if isinstance(value, (str,Path)):
            value = xr.load_dataset(Path(value))
        if isinstance(value, xr.Dataset):
            assembly = cls(0)
            assembly.data = value
            for var in assembly.data.data_vars.keys():
                dtype = assembly.data[var].attrs["dtype"]
                if 'U' in dtype:
                    if assembly.data[var].attrs["kind"] == "atoms":
                        dtype = "U3"
                    elif assembly.data[var].attrs["kind"] == "_elements":
                        dtype = "U20"
                    else:
                        dtype = "U1024" # restore long length strings
                assembly.data[var].data = np.array(assembly.data[var].data, dtype=dtype)
            if "json_attrs" in assembly.data.attrs:
                attrs = json.loads(assembly.data.attrs["json_attrs"])
                assembly.data.attrs.update(attrs)
                del assembly.data.attrs["json_attrs"]
            return assembly
        else:
            raise FileNotFoundError("Failed to load the file.") # Move to UI

    @classmethod
    def reader(cls, value:Union[str, Path], format:str = None):
        """Interface function that selects a reader depending on file format.

        See :func:`atmos.formats.filesio.reader` for more.
        """
        import atmos.formats.filesio as atmos_filesio
        return atmos_filesio.reader(value, format)

    def writer(self, filepath, quantity:Union[str, Dict[str, str]] = None, format:str = None, *, overwrite:bool = None, append:bool = None) -> None:
        """Interface function that selects a writer depending on file format.

        If file exists, user is expected to explicitly use one of
        ``overwrite = True`` or ``append = True``.

        See :func:`atmos.formats.filesio.writer` for more.
        """
        import atmos.formats.filesio as atmos_filesio
        return atmos_filesio.writer(filepath, self, quantity, format, overwrite, append)
    
    def copy(self, copy_essentials:bool = False):
        """Return a (deep) copy of the instannce.

        Parameters
        ----------
        copy_essentials : bool, optional
            Copy only essential quantities, by default False.
            Essential quantities are those required for generating the same
            geometry i.e. atoms, positions, dimensions and fixations.

        Returns
        -------
        atmos.Assembly type
            Deep copy of the *assembly*.
        """
        if copy_essentials:
            assembly_copy = self.__class__(len(self))
            assembly_copy["atoms"] = self.atoms["symbols"]
            assembly_copy["atoms"].attrs.update(self["atoms"].attrs)
            if self._has_quantity("positions"):
                assembly_copy["positions"] = self["positions"].data
                assembly_copy["positions"].attrs.update(self["positions"].attrs)
            if self.dimensions is not None:
                assembly_copy.attach("cell",    "dimensions", self["dimensions"].data)
                assembly_copy["dimensions"].attrs.update(self["dimensions"].attrs)
            if self._has_quantity("fixations"):
                assembly_copy.attach("n_vector", "fixations", self["fixations"].data, dtype=bool)
                assembly_copy["fixations"].attrs.update(self["fixations"].attrs)
            return assembly_copy
        new_assembly = self.load()
        new_assembly.data = self.data
        return new_assembly