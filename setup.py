from setuptools import setup, find_packages

import sys

required_python = (3, 9)

if sys.version_info < required_python:
    raise ImportError('ATMOS requires Python '+".".join(str(num) for num in required_python))

install_requires = [
    'xarray  >= 0.19', 
    'h5netcdf',# >= 0.11',
    'numpy   ',# >= 1.21',
    'pandas  ',# >= 1.3',
    'h5py    ',# >= 3.4',
    ]

with open('README.md', mode='r', encoding="UTF-8") as fin:
    long_description = fin.read()

setup(
    name             = 'ATMOS',
    url              = 'https://gitlab.com/NC-AFM-at-UniLeeds/atmos',
    author           = 'Shashank S. Harivyasi',
    author_email     = 's.s.harivyasi@leeds.ac.uk',
    install_requires = install_requires,
    version          = '0.1',
    license          = 'The Unlicense',
    description      = 'Atoms, Tips and Molecules over Surfaces',
    long_description = long_description,
    classifiers      = ['Development Status :: 3 - Alpha',
                        'License :: OSI Approved :: The Unlicense (Unlicense)',
                        'Programming Language :: Python :: 3.9',
                        'Topic :: Scientific/Engineering :: Physics'],
    packages         = find_packages()
)