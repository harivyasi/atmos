# ATMOS

Atoms, Tips and Molecules over Surfaces

A python package to help you when doing atomistic simulations for surface science.

[Documentation for ATMOS can be found here.](https://nc-afm-at-unileeds.gitlab.io/atmos)