Using ATMOS
===========

All functionalities of ATMOS can be access via the `Assembly class`_. It is also 
the recommended way of using it for beginners.

To use Assembly, import it in your code as follows::

   from atmos import Assembly

Then to read/load a file, you can do something like::

   my_assembly = Assembly.reader("my_cube_file.cube") # for a Gaussian cube file.
   my_assembly = Assembly.reader("geometry.in")       # for FHIaims geometry.in file.
   my_assembly = Assembly.load("previously_saved.nc") # for a saved instance of assembly.

depending on the input file format of your choice.

All data associated with such an assembly can be listed using  ``print(my_assembly.quantities)`` 
and can be accessed using ``my_assembly["quantity direction atom_id"]`` protocol. For example::

   #           index            operates on:
   my_assembly[5]               # the 6th atom 
   my_assembly["7"]             # the 8th atom 
   my_assembly[7:10]            # the 8th to 10th atom 
   my_assembly["7:10"]          # the 8th to 10th atom 
   my_assembly["charges 3"]     # charge of the 4th atom 
   my_assembly["charges 3:10"]  # charges on atoms 4th to 10th
   my_assembly["forces y 2"]    # y component of forces acting on the 3rd atom 
   my_assembly["positions z 5"] # z component of positons of the 6th atom 

In addition, you can perform some operations very easily. For example::

   #              command         does:
   my_assembly << "move 5"        # moves all the atoms by  5 Å in each of the three directions
   my_assembly << "move -5, z"    # moves all the atoms by -5 Å in the z direction
   my_assembly << "move 5, x 3"   # moves the 4th atom  by  5 Å in the x direction
   my_assembly << "freeze"        # freeze all the atoms   (constrain their relaxation)
   my_assembly << "unfreeze"      # unfreeze all the atoms (unconstrain their relaxation)
   my_assembly << "freeze, x"     # freeze all the atoms along the x direction
   my_assembly << "unfreeze, z 5" # unfreeze atom number 6 along the z direction
   my_assembly << "fractional"    # convert positions into fractional coordinate system
   my_assembly << "cartesian"     # convert positions into cartesian coordinate system

Here, the first part of the string is the command and the second part, the one 
after the comma, is the index you want to operate on (in same notation as above.)
   
Assembly class
--------------

.. automodule:: atmos.assembly
   :members:
   :undoc-members:
   :private-members:
   :special-members:
   :exclude-members: __module__, __slots__, data


Subpackages
-----------

.. toctree::
   :maxdepth: 3

   atmos.formats
   atmos.helpers
