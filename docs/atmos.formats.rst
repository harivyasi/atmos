Formats package
===============

A package to perform file io operations. All operations in this package 
can be performed directly from atmos (without importing this subpackage)
as shown in these examples::

   from atmos import Assembly
   my_assembly = Assembly.reader("path/to/file", format="aimsgeo")
   my_assembly.writer("geometry.xyz", format="xyz")

FilesIO
-------

**atmos.formats.filesio**

An interface submodule to determine file format and take care of 
general file IO modalities.

.. automodule:: atmos.formats.filesio
   :members:
   :undoc-members:
   :show-inheritance:

Cube
----
**atmos.formats.cube**

To operate on Gaussian cube files.

.. automodule:: atmos.formats.cube
   :members:
   :undoc-members:
   :private-members:
   :show-inheritance:

FHIaims
-------

**atmos.formats.fhiaims**

To operate on FHIaims geometry.in, geometry.in.next_step and output files.

.. automodule:: atmos.formats.fhiaims
   :members:
   :undoc-members:
   :private-members:
   :exclude-members: data

VASP
----

**atmos.formats.vasp**

To operate on VASP POSCAR file.

.. automodule:: atmos.formats.vasp
   :members:
   :undoc-members:
   :private-members: